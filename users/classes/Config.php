class Config {
	//Función que permite la lectura de los parametros de configuración
	public static function get($path = null){
		if($path){
			$config = $GLOBALS['config'];
			$path = explode('/', $path);

			foreach ($path as $bit) {
				if(isset($config[$bit])){
					$config = $config[$bit];
				} else {
					return false;
				}
			}

			return $config;
		}

		return false;
	}
}
