class Hash{
    //Funcion que permite crear la contraseña, usando un algoritmo sha256 y el metodo Salt para asi aumentar la seguridad de las mismas.
	public static function make($string, $salt = ''){
		return hash('sha256', $string . $salt);
	}
    //Funcion que permite implementar el salt en dichas contraseñas teniendo en cuenta la longitud de la misma. 
	public static function salt($length){
		//Se retorno el vector de inicializacion.
		return mcrypt_create_iv($length);
	}
	
    //Funcion que permite la creación de un ID.
	public static function unique(){
	    //Retorna el ID unico que se construyo de la contraseña
		return self::make(uniqid());
	}
}