<?php

$lang = array();

//Cuenta
$lang = array_merge($lang,array(
	"ACCOUNT_USER_ADDED" 		=> "Un nuevo usuario a sido agregado!",
	"ACCOUNT_SPECIFY_USERNAME" 		=> "Por favor agregue su nombre de usuario",
	"ACCOUNT_SPECIFY_PASSWORD" 		=> "Por favor agregue su contraseña",
	"ACCOUNT_SPECIFY_EMAIL"			=> "Por favor ingrese su correo electronico",
	"ACCOUNT_INVALID_EMAIL"			=> "Correo Electronico Invaliudo",
	"ACCOUNT_USER_OR_EMAIL_INVALID"		=> "Nombre de usuario o correo electronico invalido",
	"ACCOUNT_USER_OR_PASS_INVALID"		=> "Nombre de usuario o contraseña invaliudo",
	"ACCOUNT_USER_OR_PASS_INVALID2"		=> "Hemos actualizado nuestra tecnología para proteger mejor su cuenta.<a href='forgot-password.php'> Por favor, haga clic aquí para actualizar su contraseña a nuestro nuevo sistema.</a> Lo sentimos por el incidente.",
	"ACCOUNT_ALREADY_ACTIVE"		=> "Tu cuenta a sido activida exitosamente",
	"ACCOUNT_INACTIVE"			=> "Tu cuenta está inactiva. Revise sus correos electrónicos / carpeta de spam para obtener instrucciones sobre la activación de la cuenta.",
	"ACCOUNT_USER_CHAR_LIMIT"		=> "Su nombre de usuario debe estar entre m1% y m2% de caracteres en longitud",
	"ACCOUNT_DISPLAY_CHAR_LIMIT"		=> "Su nombre para mostrar debe estar entre m1% y m2% de caracteres en longitud",
	"ACCOUNT_PASS_CHAR_LIMIT"		=> "Su contraseña debe estar entre m1% y% m2 caracteres de longitud",
	"ACCOUNT_TITLE_CHAR_LIMIT"		=> "Los títulos deben estar entre m1% y m2% caracteres en longitud",
	"ACCOUNT_PASS_MISMATCH"			=> "Su contraseña y contraseña de confirmación deben coincidir",
	"ACCOUNT_DISPLAY_INVALID_CHARACTERS"	=> "El nombre para mostrar sólo puede incluir caracteres alfanuméricos",
	"ACCOUNT_USERNAME_IN_USE"		=> "El Nombre de usuario %m1% se encuentra en uso",
	"ACCOUNT_DISPLAYNAME_IN_USE"		=> "El nombre de usuario %m1% Esta en uso",
	"ACCOUNT_EMAIL_IN_USE"			=> "El correo electronico %m1% Esta en uso",
	"ACCOUNT_LINK_ALREADY_SENT"		=> "Ya se ha enviado un correo electrónico de activación a esta dirección de correo electrónico en el último %m1% hora(s)",
	"ACCOUNT_NEW_ACTIVATION_SENT"		=> "Te hemos enviado un nuevo enlace de activación por correo electrónico, consulta tu correo electrónico",
	"ACCOUNT_SPECIFY_NEW_PASSWORD"		=> "Por favor ingrese su nueva contraseña",
	"ACCOUNT_SPECIFY_CONFIRM_PASSWORD"	=> "Por favor confirme su nueva contraseña",
	"ACCOUNT_NEW_PASSWORD_LENGTH"		=> "La nueva contraseña debe estar entre %m1% and %m2% caracteres de longitud",
	"ACCOUNT_PASSWORD_INVALID"		=> "La contraseña actual no coincide con la que tenemos registrada",
	"ACCOUNT_DETAILS_UPDATED"		=> "Los detalles de la cuenta han sido actualizados",
	"ACCOUNT_ACTIVATION_MESSAGE"		=> "Deberá activar su cuenta antes de iniciar sesión. Siga el siguiente enlace para activar su cuenta. \n\n
	%m1%activate-account.php?token=%m2%",
	"ACCOUNT_ACTIVATION_COMPLETE"		=> "Ha activado correctamente su cuenta. Ahora puede ingresar <a href=\"login.php\">aquí</a>.",
	"ACCOUNT_REGISTRATION_COMPLETE_TYPE1"	=> "Se ha registrado exitosamente. Ahora puede ingresar <a href=\"login.php\">aquí</a>.",
	"ACCOUNT_REGISTRATION_COMPLETE_TYPE2"	=> "Se ha registrado exitosamente. Pronto recibirá un correo electrónico de activación.
	Debe activar su cuenta antes de iniciar sesión.",
	"ACCOUNT_PASSWORD_NOTHING_TO_UPDATE"	=> "No se puede actualizar con la misma contraseña",
	"ACCOUNT_PASSWORD_UPDATED"		=> "La contraseña de la cuenta a sido actualizada",
	"ACCOUNT_EMAIL_UPDATED"			=> "Cuenta de correo electronico actualizada",
	"ACCOUNT_TOKEN_NOT_FOUND"		=> "Token no existe / Cuenta ya está activada",
	"ACCOUNT_USER_INVALID_CHARACTERS"	=> "El nombre de usuario sólo puede incluir caracteres alfanuméricos",
	"ACCOUNT_DELETIONS_SUCCESSFUL"		=> "Ha eliminado correctamente %m1% usuarios",
	"ACCOUNT_MANUALLY_ACTIVATED"		=> "%m1%' cuenta ha sido activada manualmente",
	"ACCOUNT_DISPLAYNAME_UPDATED"		=> "El nombre a mostrar a sido cambiado %m1%",
	"ACCOUNT_TITLE_UPDATED"			=> "%m1%'s título cambiado a %m2%",
	"ACCOUNT_PERMISSION_ADDED"		=> "Se ha añadido acceso a %m1% niveles de permiso",
	"ACCOUNT_PERMISSION_REMOVED"		=> "Se ha eliminado el acceso de %m1% niveles de permisos",
	"ACCOUNT_INVALID_USERNAME"		=> "Nombre de usuario invalido",
	"CAPTCHA_ERROR"		=> "La prueba de Captcha a fallado, Robot!",
	));

?>