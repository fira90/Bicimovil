<?php

require_once $abs_us_root.$us_url_root.'usersc/includes/custom_functions.php';
require_once $abs_us_root.$us_url_root.'usersc/includes/analytics.php';

//Funcion para realizar el testeo
function testUS(){
	echo "<br>";
	echo "Función para el testing de propiedades de inclusion";
	echo "<br>";
}

//Funcion que permite enlazar el servicio de gravatar con el sistema
function get_gravatar($email, $s = 120, $d = 'mm', $r = 'pg', $img = false, $atts = array() ) {
	$url = 'https://www.gravatar.com/avatar/';
	$url .= md5( strtolower( trim( $email ) ) );
	$url .= "?s=$s&d=$d&r=$r";
	if ( $img ) {
		$url = '<img src="' . $url . '"';
		foreach ( $atts as $key => $val )
		$url .= ' ' . $key . '="' . $val . '"';
		$url .= ' />';
	}
	return $url;
}

//Función que comprueba si el permiso existe en la base de datos
function permissionIdExists($id) {
	$db = DB::getInstance();
	$query = $db->query("SELECT id FROM permissions WHERE id = ? LIMIT 1",array($id));
	$num_returns = $query->count();

	if ($num_returns > 0) {
		return true;
	} else {
		return false;
	}
}


//Función que permite comprobar si el ID del usuario existe en la db
function userIdExists($id) {
	$db = DB::getInstance();
	$query = $db->query("SELECT * FROM users WHERE id = ?",array($id));
	$num_returns = $query->count();
	if ($num_returns > 0){
		return true;
	}else{
		return false;
	}
}